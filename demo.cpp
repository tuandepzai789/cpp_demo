#include<bits/stdc++.h>
using namespace std;
//bai 12: viet chuong trinh kiem tra so thuan nghich
int main() {
	int n;
	cin>>n;
	int m=n;
	int reverse=0;
	while(n>0){
		reverse=reverse*10 + n%10;
		n/=10;
	}
	if(reverse==m){
		cout<<"la so thuan nghich";
	}else{
		cout<<"khong phai so thuan nghich";
	}
}
// bai 7: viet chuong trinh kiem tra so nguyen to
int main(){
	int n;
	cin>>n;
	bool check=true;
	if(n<2) check=false;
	for(int i=2;i<=sqrt(n);i++){
		if(n%i==0){
			check=false;
			break;
		}
	}
	if(check) cout<<n<<" la so nguyen to";
	else cout<<n<<" khong phai so nguyen to";	
}

// bai 5: nhap so nguyen n, k. hay tinh tong cac so nguyen khong lon hon n va chia het cho k
int main(){
	int n,k, sum=0;
	cin>>n>>k;
	for(int i=1;i<=n;i++){
		if(i%k==0){
			sum+=i;
		}
	}
	cout<<sum;
}

//bai 9 : nhap vao so nguyen n, hay tinh tong cac chu so cua so nguyen vua nhap vao
int main(){
	long long n, sum=0;
	cin>>n;
	while(n>0){
		sum+=n%10;
		n/=10;
	}
	cout<<"tong cac chu so cua "<<n<<"la: "<<sum;
}
